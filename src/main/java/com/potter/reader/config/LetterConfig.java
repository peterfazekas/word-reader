package com.potter.reader.config;

import com.potter.reader.domain.Letter;
import com.potter.reader.domain.LetterStyle;
import com.potter.reader.service.DataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class LetterConfig {

    @Value("${consonants}")
    private String consonants;
    @Value("${vowels}")
    private String vowels;
    @Value("${letter.style}")
    private String letterStyle;
    @Value("${word.length}")
    private int wordLength;
    @Autowired
    private DataProvider dataProvider;

    @Bean
    public Letter getLetter() {
        return new Letter(consonants, vowels, LetterStyle.valueOf(letterStyle), wordLength);
    }

    @Bean
    public Random random() {
        return new Random();
    }
}
