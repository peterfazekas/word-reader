package com.potter.reader.domain;

public enum LetterStyle {
    LOWER, UPPER, FIRST
}
