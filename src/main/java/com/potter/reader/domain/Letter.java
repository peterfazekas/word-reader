package com.potter.reader.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public final class Letter {

    private final String consonants;
    private final String vowels;
    private final LetterStyle letterStyle;
    private final int wordLength;
}
