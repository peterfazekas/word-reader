package com.potter.reader.service;

import com.potter.reader.domain.Letter;
import com.potter.reader.domain.LetterStyle;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@Service
public class WordService {

    private final DataProvider dataProvider;
    private final Random random;
    private List<String> words;

    public WordService(DataProvider dataProvider, Random random) {
        this.dataProvider = dataProvider;
        this.random = random;
    }

    public String getWord(Letter letter) {
        if (words == null) {
            readWords(letter);
        }
        return hasWord() ? formatWord(letter.getLetterStyle(), getWord()) : "-";
    }

    public void readWords(Letter letter) {
        words = new ArrayList<>(dataProvider.readWords(letter));
    }

    private String formatWord(LetterStyle letterStyle, String word) {
        switch (letterStyle) {
            case UPPER: return word.toUpperCase();
            case FIRST: return formatToFirst(word);
            default: return word.toLowerCase();
        }
    }

    private String formatToFirst(String word) {
        var sb = new StringBuilder(String.valueOf(word.toUpperCase().charAt(0)));
        IntStream.range(1, word.length())
                .mapToObj(i -> word.toLowerCase().charAt(i))
                .forEach(sb::append);
        return sb.toString();
    }

    private String getWord() {
        return words.remove(random.nextInt(words.size()));
    }

    private boolean hasWord() {
        return words.size() > 0;
    }

}
