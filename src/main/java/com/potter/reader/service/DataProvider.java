package com.potter.reader.service;

import com.potter.reader.domain.Letter;

import java.util.List;

public interface DataProvider {

    List<String> readWords(Letter letter);
}
