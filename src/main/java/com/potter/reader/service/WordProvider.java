package com.potter.reader.service;

import com.potter.reader.domain.Letter;
import com.potter.reader.exception.ReaderException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WordProvider implements DataProvider {

    private static final String URL_STRING = "http://benoke98.f.fazekas.hu/alapadatok/magyar_szavak.txt";

    public List<String> readWords(Letter letter) {
        var url = getUrl();
        try (var reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            var words = reader.lines()
                    .filter(word -> word.length() <= letter.getWordLength())
                    .filter(word -> isMatch(letter, word))
                    .collect(Collectors.toList());
            log.info("{} words selected", words.size());
            reader.close();
            return words;
        } catch (IOException e) {
            log.error("Error happened: ", e);
            throw new ReaderException(e.getMessage());
        }
    }

    private URL getUrl() {
        try {
            return new URL(URL_STRING);
        } catch (MalformedURLException e) {
            log.error("Error happened: ", e);
            throw new ReaderException(e.getMessage());
        }
    }

    private boolean isMatch(Letter letter, String word) {
        var letters = letter.getConsonants().toUpperCase() + letter.getVowels().toUpperCase();
        return word.chars()
                .mapToObj(i -> (char) i)
                .map(String::valueOf)
                .allMatch(letters::contains);
    }
}
