package com.potter.reader.service;

import com.potter.reader.domain.Letter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class SyllableService {

    private static final int ROWS = 8;
    private static final int COLUMNS = 6;

    private final Random random;

    public SyllableService(Random random) {
        this.random = random;
    }

    public List<List<String>> getSyllables(Letter letter) {
        var consonants = convertToRandomList(letter.getConsonants(), ROWS);
        var vowels = convertToRandomList(letter.getVowels(), COLUMNS);
        return consonants.stream()
                .map(getStringListFunction(vowels))
                .collect(Collectors.toList());
    }

    private Function<String, List<String>> getStringListFunction(List<String> vowels) {
        return consonant -> vowels.stream()
                .map(vowel -> consonant + vowel)
                .collect(Collectors.toList());
    }

    private List<String> convertToRandomList(String characters, int bounder) {
        var characterList = convertToList(characters);
        return IntStream.range(0, Math.min(characterList.size(), bounder))
                .map(i -> random.nextInt(characterList.size()))
                .mapToObj(characterList::remove)
                .collect(Collectors.toList());
    }

    private List<String> convertToList(String characters) {
        return characters.chars()
                .mapToObj(i -> (char) i)
                .map(String::valueOf)
                .collect(Collectors.toList());
    }

}
