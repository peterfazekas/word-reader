package com.potter.reader.exception;

public class ReaderException extends RuntimeException {

    public ReaderException(String message) {
        super(message);
    }
}
