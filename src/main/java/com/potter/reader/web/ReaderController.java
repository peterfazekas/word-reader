package com.potter.reader.web;

import com.potter.reader.domain.Letter;
import com.potter.reader.service.SyllableService;
import com.potter.reader.service.WordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Slf4j
@Controller
public class ReaderController {

    private Letter letter;
    private final SyllableService syllableService;
    private final WordService wordService;

    public ReaderController(SyllableService syllableService, WordService wordService, Letter letter) {
        this.syllableService = syllableService;
        this.wordService = wordService;
        this.letter = letter;
    }

    @PostMapping("/set")
    public String setLetters(@ModelAttribute Letter letter) {
        this.letter = letter;
        wordService.readWords(letter);
        log.info("Letter values: {}", letter.toString());
        return "redirect:/";
    }

    @PostMapping("/set-syllable")
    public String setSyllable(@ModelAttribute Letter letter) {
        this.letter = letter;
        log.info("Letter values: {}", letter.toString());
        return "redirect:/get";
    }

    @GetMapping("/")
    public String getWord(Model model) {
        var word = wordService.getWord(letter);
        log.info("The selected word is: {}", word);
        model.addAttribute("word", word);
        model.addAttribute("letter", letter);
        return "index";
    }

    @GetMapping("/get")
    public String getSyllable(Model model) {
        var syllables = syllableService.getSyllables(letter);
        log.info("The generated syllables are: {}", syllables);
        model.addAttribute("syllables_list", syllables);
        model.addAttribute("letter", letter);
        return "szotagok";
    }

}
