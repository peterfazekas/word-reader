package com.potter.reader.service;

import com.potter.reader.domain.Letter;
import com.potter.reader.domain.LetterStyle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class WordServiceTest {

    @Mock
    private DataProvider dataProvider;
    @Mock
    private Random random;
    private WordService underTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        underTest = new WordService(dataProvider, random);
    }

    @ParameterizedTest
    @CsvSource({"UPPER,test,TEST", "LOWER,TEST,test", "FIRST,TEST,Test"})
    void testGetWordShouldReturnExpectedWordWhenWordsListIsNotEmpty(String letterStyle, String input, String expected) {
        // GIVEN
        var letter = new Letter("c", "a", LetterStyle.valueOf(letterStyle), 3);
        var wordList = List.of(input);
        given(dataProvider.readWords(letter)).willReturn(wordList);
        given(random.nextInt(wordList.size())).willReturn(0);

        // WHEN
        underTest.readWords(letter);
        var actual = underTest.getWord(letter);

        // THEN
        Assertions.assertAll(
                () -> assertThat(actual, equalTo(expected)),
                () -> verify(dataProvider).readWords(letter),
                () -> verify(random).nextInt(wordList.size()),
                () -> verifyNoMoreInteractions(dataProvider, random)
        );
    }

    @Test
    void testGetWordShouldReturnNoValidWordWhenWordsListIsEmpty() {
        // GIVEN
        var letter = new Letter("c", "a", LetterStyle.LOWER, 3);
        given(dataProvider.readWords(letter)).willReturn(Collections.EMPTY_LIST);

        // WHEN
        underTest.readWords(letter);
        var actual = underTest.getWord(letter);

        // THEN
        Assertions.assertAll(
                () -> assertThat(actual, equalTo("-")),
                () -> verify(dataProvider).readWords(letter),
                () -> verifyNoMoreInteractions(dataProvider)
        );
    }

}