package com.potter.reader.service;

import com.potter.reader.domain.Letter;
import com.potter.reader.domain.LetterStyle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Random;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class SyllableServiceTest {

    private static final String CONSONANTS = "cccccc";
    private static final String VOWELS = "aaaaaa";
    private static final String SYLLABLE = "ca";

    @Mock
    private Random random;
    private SyllableService underTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        underTest = new SyllableService(random);
    }

    @Test
    void testGetSyllables() {
        // GIVEN
        var letter = new Letter(CONSONANTS, VOWELS, LetterStyle.LOWER, 3);
        given(random.nextInt(any(Integer.class))).willReturn(0);

        // WHEN
        var actual = underTest.getSyllables(letter);

        // THEN
        Assertions.assertAll(
                () -> assertThat(actual.size(), equalTo(CONSONANTS.length())),
                () -> assertThat(actual.get(0).size(),  equalTo(VOWELS.length())),
                () -> assertThat(actual.get(0).get(0),  equalTo(SYLLABLE)),
                () -> verify(random, atLeastOnce()).nextInt(any(Integer.class)),
                () -> verifyNoMoreInteractions(random)
        );
    }
}